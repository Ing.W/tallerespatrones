package co.com.validator;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ReglaFechaRango implements Regla{
    @Override
    public String execute(Transaccion transaccion) {
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-mm-dd");

        try{
            Date fechaMinima = formater.parse( "2022-01-01");
            Date fechaMaxima = formater.parse( "2022-06-01");

            if (transaccion.cuenta.fechaRegistro.before(fechaMinima)){

                return "La cuenta no supera las fecha limite";
            }

        }catch (Exception e){
            return "La cuenta no supera las fecha limite";
        }
        return "";
    }
}


package co.com.validator;

public class ReglaMontoMaximo implements Regla{
    @Override
    public String execute(Transaccion transaccion) {
        if (transaccion.montoMovimiento >= 20000){
            return "El monto de la transaccion supera el valor de 20.000";
        }
        return "";
    }
}

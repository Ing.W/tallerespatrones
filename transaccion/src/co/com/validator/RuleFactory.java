package co.com.validator;

public class RuleFactory {

    public static Regla generateRule(String ruleName){

        if ("CUENTA ACTIVA".equals(ruleName)){
            return new ReglaCuentaActiva();
        }
        if ("SALDO_SUFICIENTE".equals(ruleName)){
            return new ReglaSaldoSuficiente();
        }
        if ("MONTO_MAXIMO".equals(ruleName)){
            return new ReglaMontoMaximo();
        }
        if ("FECHA_ACTIVACION".equals(ruleName)){
            return new ReglaFechaRango();
        }
        return new ReglaCuentaActiva();

    }
}

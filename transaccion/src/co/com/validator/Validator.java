package co.com.validator;

import java.util.ArrayList;
import java.util.List;

public class Validator {

    private final List<Regla> rules;
    public Validator(List<Regla>rules){
        this.rules=rules;
        System.out.println("Constructor Validator");
    }
    List<String> validar(Transaccion transation) {
        List<String> reglasVioladas=new ArrayList<>();

        for(Regla rule: this.rules){
            String response=rule.execute(transation);
            if (!response.isEmpty()){
                reglasVioladas.add(response);
            }

        }
/*
        ReglaCuentaActiva reglaCuenta=new ReglaCuentaActiva();

        if (!reglaCuenta.execute(transation).isEmpty()){
            reglasVioladas.add(reglaCuenta.execute(transation));
        }

        ReglaSaldoSuficiente reglaSaldoSuficiente=new ReglaSaldoSuficiente();
        if (!reglaSaldoSuficiente.execute(transation).isEmpty()){
            reglasVioladas.add(reglaSaldoSuficiente.execute(transation));
        }

        ReglaMontoValido reglaMontoValido=new ReglaMontoValido();
        if (!reglaMontoValido.execute(transation).isEmpty()){
            reglasVioladas.add(reglaMontoValido.execute(transation));
        }
*/

        /*if (transation.montoMovimiento>transation.cuenta.monto){
            reglasVioladas.add("El monto de la transaccion supera su monto actual");
        }

        if (transation.montoMovimiento<=0){
            reglasVioladas.add("El monto de la transaccion debe ser mayor a 0");
        }*/
        return reglasVioladas;
    }

}

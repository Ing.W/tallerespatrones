package co.com.validator;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ReglaFechaActivacion implements Regla{

    @Override
    public String execute(Transaccion transaccion) {


        SimpleDateFormat formater = new SimpleDateFormat("yyyy-mm-dd");

        try{
            Date fechaMinima = formater.parse( "2022-01-01");

            if (transaccion.cuenta.fechaRegistro.before(fechaMinima)){

                return "La cuenta no supera las fecha minima de activacion";
            }

        }catch (Exception e){
            return "La cuenta no supera las fecha minima de activacion";
        }
        return "";
    }
}





package co.com.validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Cuenta miCuenta=new Cuenta();
        miCuenta.nombre="William";
        miCuenta.activa=false;
        miCuenta.tipo="Ahorro";
        miCuenta.monto=1000000.0;
        miCuenta.fechaRegistro =  new Date();

        Transaccion pagoSemestre=new Transaccion();
        pagoSemestre.fecha=new Date();
        pagoSemestre.montoMovimiento=30000.0;
        pagoSemestre.historico=new ArrayList<>();
        pagoSemestre.cuenta=miCuenta;


        List<Regla> rules=new ArrayList<>();

        rules.add(new ReglaMontoMaximo());


        Validator validador=new Validator(rules);
        List<String> reglasVioladas=validador.validar(pagoSemestre);

        System.out.println("CUENTA BANCARIA");
        System.out.println("Usuario: "+miCuenta.nombre+"\nMonto: "+miCuenta.monto);
        System.out.println("Fecha de transaccion: "+pagoSemestre.fecha+" \nMonto a transaccion: "+ pagoSemestre.montoMovimiento);
        System.out.println(reglasVioladas.stream().collect(Collectors.joining("\n")));
    }
    private List<Regla> basicConfig(){

        List<Regla> reglas = new ArrayList<>();
        reglas.add(new ReglaCuentaActiva());
        return reglas;
    }
    private List<Regla> mediumConfig(){

        List<Regla> reglas = new ArrayList<>();
        reglas.add(RuleFactory.generateRule("FECHA_ACTIVACION"));
        reglas.add(RuleFactory.generateRule("SALDO_SUFICIENTE"));
        return reglas;

    }
    private List<Regla> fullConfig(){

        List<Regla> reglas = new ArrayList<>();
        reglas.add(RuleFactory.generateRule("FECHA_ACTIVACION"));
        reglas.add(RuleFactory.generateRule("SALDO_SUFICIENTE"));
        reglas.add(RuleFactory.generateRule("MONTO_MAXIMO"));
        return reglas;

    }
}